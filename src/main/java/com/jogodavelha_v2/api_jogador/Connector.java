package com.jogodavelha_v2.api_jogador;

import java.io.IOException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import io.netty.handler.timeout.TimeoutException;

public abstract class Connector {
	protected Channel myChannel;
	protected Connection connection;
	protected String queueName;

	public Connector(String queueName) throws IOException,TimeoutException {
		this.queueName=queueName;
		ConnectionFactory connectionFactory = new ConnectionFactory();
		// Hostname of your rabbitmq server
		connectionFactory.setHost("localhost");
		// getting a connection
		try {
			connection = connectionFactory.newConnection();
		} catch (java.util.concurrent.TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*this will create a new channel, using an internally allocated channel number or we can say it will simply declare a queue for this channel. If queue does not exist.*/
		myChannel= connection.createChannel();

		myChannel.queueDeclare(queueName, false, false, false, null);
	}

	public void close() throws IOException, TimeoutException {
		try {
			this.myChannel.close();
		} catch (java.util.concurrent.TimeoutException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		this.connection.close();
	}
}
