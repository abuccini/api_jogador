package com.jogodavelha_v2.api_jogador.model;

public class Jogador {
	private int id;
	private String nome;
	private int qtdeVitorias;
	private int qtdeDerrotas;
	private int qtdeEmpates;
	
	public Jogador() {
	}
	
	public String getNome() {
		return nome;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getQtdeVitorias() {
		return qtdeVitorias;
	}
	public void setQtdeVitorias(int qtdeVitorias) {
		this.qtdeVitorias = qtdeVitorias;
	}
	public int getQtdeDerrotas() {
		return qtdeDerrotas;
	}
	public void setQtdeDerrotas(int qtdeDerrotas) {
		this.qtdeDerrotas = qtdeDerrotas;
	}
	public int getQtdeEmpates() {
		return qtdeEmpates;
	}
	public void setQtdeEmpates(int qtdeEmpates) {
		this.qtdeEmpates = qtdeEmpates;
	}
	
	
	public void incrementarQtdeVitorias () {
		qtdeVitorias++;
	}
	
	public void incrementarQtdeDerrotas () {
		qtdeDerrotas++;
	}

	public void incrementarQtdeEmpates () {
		qtdeEmpates++;
	}
}
