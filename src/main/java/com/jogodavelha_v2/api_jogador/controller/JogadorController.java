package com.jogodavelha_v2.api_jogador.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jogodavelha_v2.api_jogador.Producer;
import com.jogodavelha_v2.api_jogador.model.Jogador;

@Controller
public class JogadorController {
	List<Jogador> jogadores = new ArrayList<Jogador>();

	@RequestMapping(path= "/jogador/criar", method = RequestMethod.POST)
	public @ResponseBody
	Jogador criarJogador (@RequestBody Jogador jogador) throws Exception{
		jogadores.add(jogador);
		Producer.sendMessage("Novo Jogador Criado: " + jogador.getNome());
		return jogador;
	}

	@RequestMapping("/jogador/listar")
	@ResponseBody
	public List<Jogador> listarJogador(){
		return jogadores;
	}

	@RequestMapping("/jogador/listar/{id}")
	@ResponseBody
	public Jogador listarUsuarioId(@PathVariable(value="id")  int id){
		return jogadores.get(id);
	}

	@RequestMapping("/jogador/pontuar/{id}/{status}")
	@ResponseBody
	public Jogador declararStatusJogador(@PathVariable(value="id")  int id, @PathVariable(value="status")  String status){

		switch(status) { 

		case "V":	
		{jogadores.get(id).incrementarQtdeVitorias();}

		case "D":
		{jogadores.get(id).incrementarQtdeDerrotas();}

		case "E":
		{jogadores.get(id).incrementarQtdeEmpates();}

		}

		return jogadores.get(id);
	}

}